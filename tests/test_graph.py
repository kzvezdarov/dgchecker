from dgchecker import graph

import unittest
import string

TEST_GRAPH = ['AB5', 'BC4', 'CD8', 'DC8', 'DE6', 'AD5', "CE2", 'EB3', 'AE7']

ROUTES = [(['A', 'B', 'C'], 9), (['A', 'D'], 5), (['A', 'D', 'C'], 13),
          (['A', 'E', 'B', 'C', 'D'], 22), (['A', 'E', 'D'], 2)]

class TestGraph(unittest.TestCase):
    def setUp(self):
        self.graph = graph.Graph.from_list(TEST_GRAPH)

    def test_parse_edge(self):
        """
        Simply assert that edges can be pared without exceptions;
        the edge structure should enforce that the data is in the correct
        format
        """
        for raw_edge in TEST_GRAPH:
            graph.parse_edge(raw_edge)

    def test_distance(self):
        for route, distance in ROUTES:
            try:
              self.assertEqual(distance, self.graph.distance(route))
            except KeyError:
                # The fourth route in the test setup is known to be invalid.
                self.assertEqual(route, ROUTES[4][0])

    def test_stop_count(self):
        start, end = 'C', 'C'
        stops = 3

        self.assertEqual(len(list(self.graph.enumerate_paths(start, end, stops))), 2)

    def test_exact_stop_count(self):
        start, end = 'A', 'C'
        stops = 4

        self.assertEqual(len(list(self.graph.enumerate_paths(start, end, stops, True))), 3)


    def test_shortest_route(self):
        routes = [('A', 'C', 9), ('B', 'B', 9)]

        for start, end, expected in routes:
            self.assertEqual(expected, self.graph.shortest(start, end)[1])

    def test_deep_routecount(self):
        """
        Technically this is covered under the previous distance & route finding tests,
        with the filtering done as part of the command line interface. Still nice to have it,
        as it illustrates the limitations of using DFS for specifically this task.
        """
        routes = self.graph.enumerate_paths('C', 'C', max_depth=10)
        self.assertEqual(7, len(list(filter(lambda route: self.graph.distance(route) < 30, routes))))


if __name__ == "__main__":
    unittest.main()
