# Directed Graph Checker

This checker implements a few simple graph traversing and pathfinding algorithms to
illustrate some simple graph analysis - it uses iterative depth first search to discover
all possible paths in the given graph (which in the presence of cycles is infinite),
and Dijkstra's algorithm for finding the shortest paths (in terms of edge weight).

### Set up

1. Clone this repository: `git clone https://gitlab.com/kzvezdarov/dgchecker`
2. Create a virtual environment: `virtualenv -p /usr/bin/python3 dgchecker/.venv`
3. Activate the virtual environment: `source dgchecker/.venv/bin/activate`
4. Install the package in editable mode: `pip3 install --editable ./dgchecker/`
5. Run the package's tests via setup.py: `python3 setup.py test`
6. Finally, run `demo.sh` to oberve some preset scenarios or use the `dgcheck` script that
was installed in the virtual environment to test various custom scenarios.
