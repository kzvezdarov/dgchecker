from .graph import Graph
import argparse
import sys


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("graph", type=str)

    parser.add_argument("--route", type=str)
    parser.add_argument("--start", type=str)
    parser.add_argument("--stop", type=str)

    parser.add_argument("--max-stops", type=int, default=10)
    parser.add_argument("--max-distance", type=int, default=None)
    parser.add_argument("--exact", action='store_true', default=False)

    parser.add_argument("--count-stops", action='store_true')
    parser.add_argument("--show-distance", action='store_true')
    parser.add_argument("--count-trips", action='store_true')
    parser.add_argument("--shortest", action='store_true')
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    graph = Graph.from_list(map(str.strip, args.graph.split(',')))

    if args.route and args.show_distance:
        try:
          print(graph.distance(args.route.split("-")))
          sys.exit(0)
        except KeyError:
            print("No such route")
            sys.exit(0)
    else:
        if args.count_trips:
            if args.max_distance is not None:
                print(len(list(filter(lambda path: graph.distance(path) < args.max_distance,
                                 graph.enumerate_paths(args.start, args.stop, args.max_stops, args.exact)))))
                sys.exit(0)

        elif args.count_stops:
            print(len(list(graph.enumerate_paths(args.start, args.stop, args.max_stops, args.exact))))
            sys.exit(0)

        if args.shortest:
            shortest_path, distance = graph.shortest(args.start, args.stop)
            print(distance)
            sys.exit(0)

    print("Nothing to do")


if __name__ == "__main__":
    main()
