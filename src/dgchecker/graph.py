from typing import List, NamedTuple, Dict, Type
from collections import UserDict, deque
from itertools import count
from heapq import heappop, heappush
import math


class Edge(NamedTuple):
    """
    A representation of the edge between two nodes.
    """
    source: str
    dest: str
    length: int


def parse_edge(edge: str) -> Type[Edge]:
    """
    A simple parser to extract an edge representation from text parameters passed
    on the command line.

    The assumptions that must hold are that each edge in text form consists of two
    letters and a number, which is enforced at runtime via assertions.

    :param edge: the raw edge string that needs to be parsed.
    """
    assert len(edge) == 3, f"Improperly formatted route {route}"

    return Edge(edge[0], edge[1], int(edge[2]))


def trace(pathmap: Dict[str, str], start: str, end: str):
    """
    Tracks the shortest path between two nodes given a path dictionary
    as produced by the Dijkstra algorithm.

    Since the path gets reconstructed in reverse, the result has to be flipped
    as well.

    :param pathmap: a single-level dict mapping node to the node it was reached from,
    essentially providing a breadcrumb trail that has to be reversed to obtain the
    actual path.
    :param start: the starting node.
    :param end: the terminal node.
    """

    def revtrack(start: str, end: str):
        while end != start:
            yield end
            end = pathmap[end]
        yield start

    return list(revtrack(start, end))[:-1]


class Graph(UserDict):
    """
    Graph in adjacency list-like representation form. The `data` member
    is a dictionary whose keys are node names and whose values are dictionaries
    containing the given node's neighbors. The value at each sub-dictionary is the
    edge itself, containing the full metadata (source, destination, length of edge).

    This representation is much more space efficient than a full on adjacency matrix, and
    works significantly better for the type of analysis that is required, since neighbors can
    be accessed very quickly and cleanly.
    """

    @classmethod
    def from_list(cls, edges: List[str]):
        return cls(map(parse_edge, edges))

    def __init__(self, edges: List[Edge]):
        super(Graph, self).__init__()

        for edge in edges:
            self.data.setdefault(edge.source, {})[edge.dest] = edge

    def distance(self, path: List[str]) -> int:
        """
        Calculates the distance of a given path.

        NOTE: Since the zip call's inputs are slices of the original input array,
        it means that this function is creating two entire copies of path. If path were
        large enough, this could result in unneccessarily large memory consumption. It can
        be remedied by taking an iterator slice instead (via islice) if needed.

        :param path: a list of nodes in the order of which they are visited.
        """
        return sum((self[source][dest].length
                    for source, dest in zip(path[:-1], path[1:])))

    def enumerate_paths(self,
                        start: str,
                        end: str,
                        max_depth: int = None,
                        strict: bool = False) -> List[str]:
        """
        Enumerates over all paths between the given nodes. Since the graph can
        contain cycles, this is potentially an infinite number of paths.

        :parma start: the starting node.
        :param end: the terminal node.
        :param max_depth: the depth up to which to search for paths.
        :param strict: only return paths matching the depth exactly.
        """
        if strict:
            return self._dfs(start, end, max_depth)
        else:
            return self._iter_deep_dfs(start, end, max_depth)

    def _iter_deep_dfs(self, start: str, end: str, max_depth: int = None) -> List[str]:
        """
        Iterative depth-first search simply iterates over discovering paths
        of different (node) length. It may seem wasteful, since multiple paths
        get visited multiple times, but that can be easily fixed by memoizing
        the depth first search function call (possibly via functools.lru_cache,
        however that would also require to reimplement this graph as an immutable
        structure instead of a dictionary, which is beyond the scope of this project).

        This function will yield paths from depth 1 to infinity, unless the maximum depth
        parameter is set.

        :param start: the starting node.
        :param end: the terminal node.
        :param max_depth: the maximum depth to iterate to; if None this generator will return
        an infinite number of paths given a cyclic graph.
        """
        for depth in count(start=1):
            yield from self._dfs(start, end, depth)

            if max_depth is not None and depth == max_depth:
                return

    def _dfs(self, start: str, end: str, depth: int) -> List[str]:
        """
        Depth-limited depth first search. It will explore up to `depth` and only
        yield a path if the terminal node is the required destination.

        The limit effectively eliminates the drawbacks usually associated with DFS and
        lets us handle directed cyclic graphs. Of course, that creates the drawback of
        coming up with a depth limit. However, given the prescribed usage of this program
        (finding routes up to/exactly of N stops/distance), the depth limit not only fits
        naturally but also becomes quite useful.

        This is an iterative implementation since Python does not optimize tail calls.
        The stack is modeled via a deque of (node, node depth, path to node) of tuples.
        The depth and path need to be stored alongside the node in order to allow backtracking
        in the iterative implementation - in a recursive implementation, shared path/depth state
        would work just fine.

        :param start: the starting node.
        :param end: the terminal node.
        :param depth: the depth limit for this run.
        """
        nodes = deque([(start, depth, [])])

        # The algorithm has to explore all available edges in order
        # to discover all available paths; hence it runs until every depth-terminal
        # node has been reached.
        while len(nodes):
            node, node_depth, node_path = nodes.pop()

            # Since program usage usually requires paths between two specific points,
            # as opposed to all paths reachable from a given starting point,
            # the path "terminator" here is strict - needs to both be the correct
            # level __and__ be the correct node, otherwise the search continues.
            if node_depth == 0 and node == end:
                yield node_path + [node]
            elif node_depth > 0:
                # The search space expansion is simply the addition of the node's
                # children to the search stack. Note that depth descent and path expansion
                # is also set at this point, just as if it was an actual recursive function call.
                nodes.extend([(child, node_depth - 1, node_path + [node])
                              for child in self[node].keys()])

    def _dijkstra(self, start: int) -> (Dict[str, int], Dict[str, str]):
        """
        Simple Dijkstra shortest path implementation. The only modification over
        the standard algorithm is that edges leading to the start node explicitly
        overwrite its own distance to itself (0).

        Note that this will return all shortest paths from the current node

        :param start: the starting node.
        """

        # This will be used as a heap, and since heapq operations sorting on
        # tuples by default assume that the first element is the sorting key,
        # it is convenient to have the lengh first and node name second.
        nodes = [(0, start)]
        distance = {start: 0}
        pathmap = {}

        while len(nodes):
            _, node = heappop(nodes)

            for child in self[node].keys():
                alt = distance.get(node, math.inf) + self[node][child].length

                # Note that in addition to the classic "alternate route" check,
                # this piece also checks if the child is the start node itself,
                # so that the algorithm handles circling back to its origin.
                if alt < distance.get(child, math.inf) or child == start:
                    distance[child] = alt
                    pathmap[child] = node
                    heappush(nodes, (alt, child))

        return distance, pathmap

    def shortest(self, start: str, end: str) -> (List[str], int):
        """
        Returns the shortest path between two nodes and its distance.

        :param start: the starting node.
        :parma end: the terminal node.
        """
        distance, pathmap = self._dijkstra(start)

        path = trace(pathmap, start, end)

        return path, distance[end]
