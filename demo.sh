#!/bin/bash

dgcheck --route "A-B-C" --show-distance "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
dgcheck --route "A-D" --show-distance "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
dgcheck --route "A-D-C" --show-distance  "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
dgcheck --route "A-E-B-C-D" --show-distance "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
dgcheck --route "A-E-D" --show-distance "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
dgcheck --count-stops --start C --stop C --max-stops 3 "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
dgcheck --count-stops --exact --start A --stop C --max-stops 4 "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
dgcheck --shortest --start 'A' --stop 'C' "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
dgcheck --shortest --start 'B' --stop 'B' "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
dgcheck --count-trips --start C --stop C --max-distance 30 "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"
