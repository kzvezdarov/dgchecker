from setuptools import setup, find_packages

from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="dgchecker",
    version="0.1",
    description="A script that performs path searches on directed graphs.",
    long_description=long_description,
    author="Kiril Zvezdarov",
    packages=find_packages("src"),
    package_dir={"": "src"},
    test_suite='tests',
    include_package_data=True,
    install_requires=[],
    extra_requires=[],
    entry_points={
        "console_scripts": [
            "dgcheck=dgchecker:main"
        ]
    })
